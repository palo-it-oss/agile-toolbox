# Team organisation Miro template

You can consume this template in Miro by importing the `*.rtb` file.

Don't hesitate to make suggestions to improve this template.

![Team Organisation image](./Team%20Organisation.jpg)
