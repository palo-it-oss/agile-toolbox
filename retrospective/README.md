# Retrospective Miro template

You can consume this template in Miro by importing the `*.rtb` file.
This work is heavily inspired by :

- Esther Derby's five stage of a successful retrospective :
  - Set the stage
  - Gather data
  - Generate insights
  - Decide what to do
  - Close the retrospective
- Spotify's [squad health check](https://engineering.atspotify.com/2014/09/squad-health-check-model/)
- Fun restrospective's [activities](https://www.funretrospectives.com)

Don't hesitate to make suggestions to improve this template.

![Retrospective image](./Team%20retrospective.jpg)
