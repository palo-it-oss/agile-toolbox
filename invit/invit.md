# Events invitations

## Sprint Planning

🔑 **Objectives:** Understand what could be achieve to increase product value in the next 3 weeks.

🙋 **People:**

- Required: Dev Team 👨‍💻; PO 🧑‍🎓
- Optional: Team Facilitator 🧙‍♂️; Experts 🧑‍🎨

📌 **Format:**

- Timebox: 3 hours max ⏱️
- PO presents the Sprint Objective and its articulations with the Product Goal.
- Then, PO presents candidate Product Backlog Items to all team by ensuring the global comprehension of stakes. For each Product Backlog Item:
  - Discussions help the team to clarify any point and increase understanding and confidence
  - Dev Team set the complexity level of Product Backlog Item by assigning them Story Points
- At the end of this meeting, the team should be agree on a first scope for the Sprint. The scope includes Sprint Objective and selected Product Backlog Items.

📦 **Deliveries:**

- Sprint Objective (Why)
- Sprint Backlog with ready User Stories (What)

🛡️ **Traps to avoid:**

- A lack of consistency for the scope of the sprint.

🔗 [Sprint Planning](https://scrumguides.org/scrum-guide.html#sprint-planning) in the Scrum Guide

🤔 All our meetings container & content may be challenged according to our needs. As usual, feedbacks are welcome.

## Daily

🔑 **Objectives:** Inspect progress and adjust our plan to achieve sprint goal

🙋 **People:**

- Required: Dev Team 👨‍💻
- Optional: Team Facilitator 🧙‍♂️; PO 🧑‍🎓; Experts 🧑‍🎨

📌 **Format:**

- Timebox: 15 minutes max ⏱️
- Walk the sprint board (mettre ici le lien vers le board que vous utilisez) and review what can be achieved today
- Review items from right to left to know what's missing to move it towards to the done state
- After this review, anyone can share an information for team awareness

📦 **Deliveries:**

- Team alignment on the plan to achieve the sprint goal, focused on high priority items
- Identify impediments
- Quick decisions

🛡️ **Traps to avoid:**

- This is not a reporting meeting, don’t do an activity report unless it helps the team to adapt the plan
- If a point need to be delve into, this should be done in a dedicated meeting

🔗 [Daily Scrum](https://scrumguides.org/scrum-guide.html#daily-scrum) in the Scrum Guide

🤔 All our meetings container & content may be challenged according to our needs. As usual feedbacks are welcome.

## Sprint Review

🔑 **Objectives:** Inspect the outcome of the Sprint and determine future adaptation.

🙋 **People:**

- Required: Dev Team 👨‍💻; PO 🧑‍🎓; Stakeholders 🤴
- Optional: Team Facilitator 🧙‍♂️; Experts 🧑‍🎨

📌 **Format:**

- Timebox: 2 hours max ⏱️
- Team presents, on Staging environment, the results of their work and focus on what will change in the next release.
- Team collects feedbacks from attendees and, potentially, adjust Product Backlog in consequence.

📦 **Deliveries:**

- Product Backlog adjustments (optional, depending on attendees feedbacks).

🛡️ **Traps to avoid:**

- Limit the review to the demo and omit discussions around attendees feedbacks.

🔗 [Sprint Review](https://scrumguides.org/scrum-guide.html#sprint-review) in the Scrum Guide

🤔 All our meetings container & content may be challenged according to our needs. As usual, feedbacks are welcome.

## Sprint Retrospective

🔑 **Objectives:** Plan ways to increase quality and effectiveness by learning, thinking and deciding together

🙋 **People:**

- Required: Dev Team 👨‍💻; PO 🧑‍🎓; Team Facilitator 🧙‍♂️
- Optional: Experts 🧑‍🎨

📌 **Format:**

- Timebox: 2 hours max ⏱️
- Based on [The Heart of Team Improvement](https://www.youtube.com/watch?v=G8T-TUo_9V4&t=135s) from Esther Derby, this is the 5 steps of the retrospectives
  - The Facilitator (not necessarily the 🧙‍♂️) introduces the session. (Set the Stage)
  - Team gathers data to share the same facts. (Gather Data)
  - Team generates insights about the situation and what it might does to change it. (Generate Insights)
  - Team decides what to do. (Decide What to Do)
  - The Facilitator close the session. (Close the Restrospective)

📦 **Deliveries:**

- Some actionable points that are planned in the next sprint.

🛡️ **Traps to avoid:**

- Missing of psychology security
- Judgements, accusations and individualization of responsibilities

🔗 [Sprint Retrospective](https://scrumguides.org/scrum-guide.html#sprint-retrospective) in the Scrum Guide

🔗 "_Regardless of what we discover, we understand and truly believe that everyone did the best job they could, given what they knew at the time, their skills and abilities, the resources available, and the situation at hand._" - Norm Kerth, *Project Retrospectives*

🤔 All our meetings container & content may be challenged according to our needs. As usual, feedbacks are welcome.
