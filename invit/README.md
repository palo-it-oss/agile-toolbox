# Invitation template to scrum events

These are suggestions to create invit to your scrum events. Fill free to modify which part you need (like the timeboxes)

For consistency, each event keep the same structure.

Each item comes from the [SCRUM Guide](https://scrumguides.org/scrum-guide.html). Don't hesitate to make suggestions to
improve theses templates.
